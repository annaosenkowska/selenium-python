from selenium.webdriver.common.by import By


class messagesPage:
    # zmienne dostępne dla wszystkich metod w danej klasie
    URL = 'http://demo.testarena.pl/zaloguj'

    # konstruktor (metoda służąca do tworzenia obiektu tej klasy)
    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL)

    def click_messages(self):
        self.browser.find_element(By.CSS_SELECTOR, ".top_messages").click()
