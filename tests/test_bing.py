from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_bing():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get("http://bing.com")

    searchbox_input = browser.find_element(By.CSS_SELECTOR, "#sb_form_q")
    search_button = browser.find_element(By.CSS_SELECTOR, "#search_icon")
    # Znalezienie paska wyszukiwania
    # Znalezienie guzika wyszukiwania (lupki)

    # Asercje że elementy są widoczne dla użytkownika
    assert searchbox_input.is_displayed() is True
    assert search_button.is_displayed() is True

    # Szukanie Vistula University
    searchbox_input.send_keys("Vistula University")
    search_button.click()

    # Sprawdzenie że lista wyników jest dłuższa niż 2
    search_results = browser.find_elements(By.CSS_SELECTOR, ".b_algo")
    assert len(search_results) > 2

    # Sprawdzenie czy w pierwszym elemencie jest Vistula
    first_result = browser.find_element(By.CSS_SELECTOR, '.b_topTitle')
    text_inside_element = first_result[0].text
    assert text_inside_element == 'Home - Vistula University'

    # Zamknięcie przeglądarki
    browser.quit()