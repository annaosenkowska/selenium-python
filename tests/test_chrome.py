import time

from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver


def test_example():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.google.com/")
    assert "Google" in driver.title
    driver.quit()


def test_Bing():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.bing.com/")
    assert "Bing" in driver.title
    driver.quit()

def test_Bing_fullscrean():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.bing.com/")
    driver.fullscreen_window()
    driver.save_screenshot("screenshot.jpg")
    assert "Bing" in driver.title
    time.sleep(2)
    driver.quit()

