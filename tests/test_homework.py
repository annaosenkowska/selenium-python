import pytest
import string
import random

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.login_page import LoginPage
from pages.cocpit_page import messagesPage


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    # stworzenie obiektu klasy (page objectu) LoginPage'a
    login_page = LoginPage(browser)
    # wywołanie metod na obiekcie klasy
    login_page.load()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")
    yield browser
    browser.quit()


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


def test_logout_correctly_displayed(browser):
    assert browser.find_element(By.CSS_SELECTOR, '[title=Wyloguj]').is_displayed() is True


def test_open_messages_tab(browser):
    messages = messagesPage(browser)
    messages.click_messages()
    panel = browser.find_element(By.CSS_SELECTOR, "#list-message")
    assert panel.is_displayed()


def test_opens_messages(browser):
    messages = messagesPage(browser)
    messages.click_messages()
    wait = WebDriverWait(browser, 10)
    selector = (By.CSS_SELECTOR, '.j_msgResponse')
    panel_with_messages = wait.until(EC.element_to_be_clickable(selector))
    assert panel_with_messages.is_displayed()


def test_open_administrator_tools(browser):
    admin_button = browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
    admin_button.click()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'


def test_create_projects(browser):
    admin_button = browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
    admin_button.click()
    create_project = browser.find_element(By.CSS_SELECTOR, '.button_link')
    create_project.click()
    random_string = get_random_string(6)
    browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_string)
    browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_string)
    browser.find_element(By.CSS_SELECTOR, '#save').click()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Właściwości projektu'


def test_create_projects_and_find_project(browser):
    admin_button = browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
    admin_button.click()
    create_project = browser.find_element(By.CSS_SELECTOR, '.button_link')
    create_project.click()
    random_string = get_random_string(6)
    browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_string)
    browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_string)
    browser.find_element(By.CSS_SELECTOR, '#save').click()
    browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()
    browser.find_element(By.CSS_SELECTOR, '#search').send_keys(random_string)
    browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()
    search_result = browser.find_element(By.LINK_TEXT, random_string)
    assert search_result.is_displayed() is True
