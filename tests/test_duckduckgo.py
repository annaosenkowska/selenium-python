from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_duckduckgo():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get("http://duckduckgo.com")

    searchbox_input = browser.find_element(By.CSS_SELECTOR, "#searchbox_input")
    search_button = browser.find_element(By.CSS_SELECTOR, ".searchbox_iconWrapper__suWUe")
    # Znalezienie paska wyszukiwania
    # Znalezienie guzika wyszukiwania (lupki)

    # Asercje że elementy są widoczne dla użytkownika
    assert searchbox_input.is_displayed() is True
    assert search_button.is_displayed() is True

    # Szukanie Vistula University
    searchbox_input.send_keys("Vistula University")
    search_button.click()

    # Sprawdzenie że lista wyników jest dłuższa niż 2
    search_results = browser.find_elements(By.CSS_SELECTOR, ".nrn-react-div")
    assert len(search_results) > 2

    # Sprawdzenie czy w pierwszym elemencie jest Vistula
    first_result = browser.find_element(By.CSS_SELECTOR, '#r1-0 h2 span')
    text_inside_element = first_result.text
    assert text_inside_element == 'Home - Vistula University'

    # Zamknięcie przeglądarki
    browser.quit()

